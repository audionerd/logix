---
title: I/O time wristband individual design
---

## logbook entry

In the world I am right now travelers are relating themselves to a new time-space-concept-. They call it IOT: input-output-time. With my card10 I can always see 1 shared time that counts every hour we are in this community in a still very linear way. But besides this I have my own input-output-time cycle. 
As for me its totally different to spent my time with my mind in virtual space or with my body in physical space, I programmed my card10 to a time cycle that takes my movement as an input and goes own with my time-cycle if I physically move around. With my ongoing time the colors of my card10s general LEDs slowly shift from light yellow to violet.
Today I met a traveler that had an interesting perspective on his own rhythm. Because they feel insecure about the amount of time they want to spent on their own to rest, they programed their card10 to a cycle that goes on to the next unit of time when they shake their card10 heavily. So they decided to shake their arm for every "moment" they experienced and aimed to rest every 10 moments for some time in camp mode. I hope to meet them again to see how he feels with this time :) 
I just loaded a pulse-input-time cycle in a village where all local travelers shared this pulse-time. I will join into this time now. It is an interesting new way of relating my time cycle to my physical being. 
getting to know it more, card10 can be individualized also in physical ways. I found some interhacktions today that one can connect through the wristband. The traveler that introduced me to this had some really weird looking interhacktions on their card10 and I recognized their individualized design on the faceplate of their card10.

## conclusions for this interhacktion:

We will have set a new shared time starting with the opening talk, counting in hours. The device will have had the functionality to have one or more personal time-cycles that have different input and outputs. The device will have had a extensionable wristband and a faceplate that might have been constructed in a very easy way so many travelers can design their own one. 

#### demands
* find out about how the wristband will have been built
* experiment with different inputs and time outputs, develope a usable personal time cycle interhacktion for the device.
* find more information on how those new time concepts
* find more details on the faceplate
