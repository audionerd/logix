---
title: Current Release
---
# Currently: CCCauliflower (day 2 - 00:30
https://card10.badge.events.ccc.de/release/card10-v1.3-cccauliflower.zip

# Update the firmware release on your card10
* download the current firmware `.zip` file to your computer
* extract it
* put your card10 into [USB storage mode](/en/gettingstarted#usb-storage)
* copy over the files you just extracted directly into the main folder
on mac devices it is recommended to use the terminal, e.g. for the cccauliflower release:
'cp -r Downloads/card10-v1.3-cauliflower/* /Volumes/CARD10/'
* eject your device (if you're doing this in the command line: don't forget the `sync` on linux)
* switch your card10 off and on again

# Previous Releases
## Broccoli (day 1 - 19:00)
https://card10.badge.events.ccc.de/release/card10-v1.2-broccoli.zip


## Asparagus
As of Day 1, 18:00
Asparagus is the firmware that is loaded on the card10s handed out on day 1 from 18:00
There are currently no previous releases



